package uz.pdp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppPractiseApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppPractiseApplication.class, args);
    }

}
